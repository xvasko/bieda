export const FETCH_ARTICLES_REQUEST = 'FETCH_ARTICLES_REQUEST';
export const FETCH_ARTICLES_SUCCESS = 'FETCH_ARTICLES_SUCCESS';
export const FETCH_ARTICLES_FAILURE = 'FETCH_ARTICLES_FAILURE';

export function fetchArticlesRequest() {
  return {
    type: FETCH_ARTICLES_REQUEST,
  }
}

export function fetchArticlesSuccess(articles) {
  return {
    type: FETCH_ARTICLES_SUCCESS,
    articles: articles,
  }
}

export function fetchArticlesFailure(e) {
  return {
    type: FETCH_ARTICLES_FAILURE,
    error: e.message,
  }
}
