import { combineReducers } from 'redux';
import { articles } from './articles';
import { isFetchingArticles } from './isFetchingArticles';

export const appReducer = combineReducers({
  articles,
  isFetchingArticles,
});