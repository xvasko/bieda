import {
  FETCH_ARTICLES_REQUEST,
  FETCH_ARTICLES_SUCCESS,
  FETCH_ARTICLES_FAILURE,
} from '../actions/actionCreators';


export const isFetchingArticles = (state = false, action) => {
  switch (action.type) {
    case FETCH_ARTICLES_REQUEST:
      console.log('isFetchingArticles START');
      return true;
    case FETCH_ARTICLES_SUCCESS:
    case FETCH_ARTICLES_FAILURE:
      console.log('isFetchingArticles END');
      return false;
    default:
      console.log('isFetchingArticles Reducer fired!');
      return state;
  }
};