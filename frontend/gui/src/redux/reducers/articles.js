import {
  FETCH_ARTICLES_REQUEST,
  FETCH_ARTICLES_SUCCESS,
  FETCH_ARTICLES_FAILURE,
} from '../actions/actionCreators';


export const articles = (state = [], action) => {
  switch (action.type) {
    case FETCH_ARTICLES_REQUEST:
      console.log('fetch request');
      return state;
    case FETCH_ARTICLES_SUCCESS:
      console.log('fetch success');
      return action.articles;
    case FETCH_ARTICLES_FAILURE:
      console.log('fetch failure');
      return state;
    default:
      console.log('articles Reducer fired!');
      return state;
  }
}