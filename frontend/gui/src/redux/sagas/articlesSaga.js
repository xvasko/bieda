import { call, put, takeEvery } from 'redux-saga/effects';
import {
  fetchArticlesSuccess,
  fetchArticlesFailure,
} from '../actions/actionCreators';
import { 
  FETCH_ARTICLES_REQUEST,
} from '../actions/actionCreators';

const api = 'http://127.0.0.1:8000/api/';

function* fetchArticles() {
  try {

    const response = yield call(fetch, api);
    const articles = yield response.json();
    yield put(fetchArticlesSuccess(articles));
  } catch (e) {
    yield put(fetchArticlesFailure(e));
  }
}

function* saga() {
  yield takeEvery(FETCH_ARTICLES_REQUEST, fetchArticles);
}

export default saga;
 