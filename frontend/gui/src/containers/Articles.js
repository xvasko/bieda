import React from 'react';
import Article from '../components/Article';


class Articles extends React.Component {

  componentDidMount() {
    this.props.fetchArticlesRequest();
  }

  render() {
    return (
      <Article data={this.props.articles}/>
    );
  }
}


export default Articles;