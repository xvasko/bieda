import React from 'react';
import { List } from 'antd';


const Article = (props) => {
  return (
    <div>
      <h3 style={{ margin: '16px 0' }}>Small Size</h3>
      <List
        size="small"
        bordered
        dataSource={props.data}
        renderItem={item => <List.Item>{item.title}</List.Item>}
      />
    </div>
  );
}

export default Article;