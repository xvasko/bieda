import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import * as serviceWorker from './serviceWorker';

import 'antd/dist/antd.css';

import CustomLayout from './containers/Layout';
import Articles from './containers/Articles.container';
import About from './containers/About';

import store from './store';


const router = (
  <Provider store={store}>
    <Router>
      <CustomLayout>
        <Route exact path='/' component={Articles} />
        <Route exact path='/about' component={About} />
      </CustomLayout>
    </Router>
  </Provider>
);

ReactDOM.render(router, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
