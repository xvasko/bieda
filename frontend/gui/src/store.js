import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { appReducer } from './redux/reducers/appReducer';
import articlesSaga from './redux/sagas/articlesSaga';

const defaultState = {
  articles: [{ title: 'articlex' }, { title: 'asdasd' }],
  isFetchingArticles: false,
};

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  appReducer,
  defaultState,
  composeEnhancers(applyMiddleware(sagaMiddleware))
);

// export const history = syncHistoryWithStore(browserHistory, store);
sagaMiddleware.run(articlesSaga);

export default store;